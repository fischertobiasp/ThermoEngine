# Notes on building Knative functions 
- Builds are managed by gitlab-ci.yml file in repository root
- Dockerfile for minimal ThermoEngine container
    + Built from Minimal Jupyter notebook Dockerfile (without notebook)
    + And, non-interactive components of the principal Dockerfile
- App and configuration Dockerfile in Geobarometer folder 

function test (GET protocol):  

http://0.0.0.0:8080?SiO2=77.5&TiO2=0.08&Al2O3=12.5&Fe2O3=0.207&Cr2O3=0.0&FeO=0.473&MnO=0.0&MgO=0.03&NiO=0.0&CoO=0.0&CaO=0.43&Na2O=3.98&K2O=4.88&P2O5=0.0&H2O=10.0&fO2%20offset=0.0